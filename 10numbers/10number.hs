printNumbers 0 = return ()
printNumbers n =
 do
  print n
  printNumbers (n-1)

main = printNumbers 10
